# yum.rb
# Copyright (C) 2010 Red Hat, Inc.

# Authors:
#   Akira TAGOH  <tagoh@redhat.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'ruby-stemp'
require 'stemp'

require 'fileutils'
require 'optparse'
require 'shellwords'
require 'tmpdir'
begin
  require 'fontpackages/compat'
rescue LoadError
  require File.join(File.dirname(__FILE__), 'comps')
end


class OptionParser

  module Arguable

    alias :orig_options :options

    def options
      @yum_config ||= []
      orig_options do |opt|
        opt.on('-C', '--cache', '[YUM] run from cache only') {|v| @yum_config << '-C'}
        opt.on('--enablerepo=REPO', '[YUM] enable one or more repositories (wildcards allowed)') {|v| @yum_config << build_yumopt(:enablerepo, v)}
        opt.on('--disablerepo=REPO', '[YUM] disable one or more repositories (wildcards allowed)') {|v| @yum_config << build_yumopt(:disablerepo, v)}

        yield opt
      end
    end # def options

    def yum_options
      @yum_config
    end # def yum_options

    private

    def build_yumopt(key, val)
      sprintf("--%s=%s", key, val.shellescape)
    end

  end # module Arguable

end # class OptionParser

module FontPackages

  class YumRepos

    def initialize(yumopts)
      @yumopts = yumopts
      @query_format = ""
      @ignore_error = false
    end # def initialize

    attr_accessor :query_format, :ignore_error

    def query(name, &block)
      repoquery([yum_options, "-q", @query_format.empty? ? "" : sprintf("--qf=%s", @query_format), name], &block)
    end # def query

    def packagelist(name, &block)
      repoquery([yum_options, "-l", name], &block)
    end # def packagelist

    def download(name)
      tmpdir = nil
      nvra = nil
      begin
        old_qf = query_format
        self.query_format = "%{name}-%{version}-%{release}.%{arch}"
        query(name) do |ret|
          nvra = ret
          break
        end
      ensure
        self.query_format = old_qf
      end
      if nvra.nil? then
        e = RuntimeError.new(sprintf("No such packages: %s", name))
        if ignore_error then
          STDERR.printf("E: %s\n", e.message)
          return
        else
          raise e
        end
      end
      if block_given? then
        tmpdir = STemp.mkdtemp(File.join(Dir.tmpdir, sprintf("%sXXXXXXXX", name)))
      end
      cwd = Dir.pwd
      begin
        Dir.chdir(tmpdir) unless tmpdir.nil?
        cmd = sprintf("yumdownloader %s %s > /dev/null 2>&1", yum_options, nvra)
        STDERR.printf("D: %s\n", cmd) if $DEBUG
        system(cmd)
        rpm = sprintf("%s.rpm", nvra)
        unless File.exist?(rpm) then
          e = RuntimeError.new(sprintf("Unable to download rpm: %s", nvra))
          if ignore_error then
            STDERR.printf("E: %s\n", e.message)
          else
            raise e
          end
        end
        yield self, rpm
      ensure
        FileUtils.rm_rf(tmpdir) unless tmpdir.nil?
        Dir.chdir(cwd)
      end
    end # def download

    def extract(name)
      download(name) do |x, rpm|
        cmd = sprintf("rpm2cpio %s | cpio -id > /dev/null 2>&1", rpm)
        STDERR.printf("D: %s\n", cmd) if $DEBUG
        system(cmd)
        yield x, rpm
      end
    end # def extract

    private

    def yum_options
      if @yumopts.kind_of?(Array) then
        @yumopts.join(' ')
      elsif !@yumopts.nil? then
	@yumopts
      else
        ""
      end
    end # def yum_options

    def repoquery(opts)
      cmd = sprintf("repoquery %s 2> /dev/null", opts.join(' '))
      STDERR.printf("D: %s\n", cmd) if $DEBUG
      IO.popen(cmd) do |f|
        until f.eof? do
          s = f.gets
          yield s.chomp unless s.nil?
        end
      end
    end # def repoquery

  end # class YumRepos

end # module FontPackages
