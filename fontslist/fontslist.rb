#! /usr/bin/env ruby
# -*- encoding: utf-8 mode: ruby -*-
# fontslist.rb
# Copyright (C) 2010 Red Hat, Inc.

# Authors:
#   Akira TAGOH  <tagoh@redhat.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'optparse'
begin
  require 'fontpackages/fontpackages'
rescue LoadError
  require File.join(File.dirname(__FILE__), '..', 'fontpackages', 'fontpackages')
end
begin
  require 'fontpackages/yum'
rescue LoadError
  require File.join(File.dirname(__FILE__), '..', 'fontpackages', 'yum')
end


yum_opts = nil
begin
  ARGV.options do |opt|
    opt.banner = sprintf("Usage: %s [options] <comps file>", File.basename(__FILE__))
    opt.parse!

    subargv = opt.order(ARGV)
    if subargv.length == 0 then
      puts opt.help
      exit
    end
  end
  yum_opts = ARGV.yum_options
rescue => e
  p e
  exit
end

pkg2lang = {}
lang2pkg = {}

y = FontPackages::YumRepos.new(yum_opts)
fp = FontPackages::FontPackages.new(ARGV[0])
i = 0
fp.fontpackages(:default).sort.each do |pkg|
  next if pkg2lang.include?(pkg.name)
  STDERR.printf("%s\n", pkg.name)
  pkg2lang[pkg.name] = fp.supported_languages(pkg)
  sans = []
  serif = []
  monospace = []
  other = []
  STDERR.printf("  Downloading rpm...\n")
  y.extract(pkg.name) do |x, rpm|
    Dir.glob(File.join('etc', 'fonts', 'conf.d', '*')) do |f|
      STDERR.printf("  Checking a fontconfig file %s...\n", f)
      fn = File.basename(f)
      tfn = File.join('usr', 'share', 'fontconfig', 'conf.avail', fn)
      priority = fn.sub(/\A(\d+).*/, '\1').to_i
      s = File.open(tfn).read
      unless s.grep(/<family>sans-serif<\/family>/).empty? then
        sans << [priority, pkg.name]
      end
      unless s.grep(/<family>serif<\/family>/).empty? then
        serif << [priority, pkg.name]
      end
      unless s.grep(/<family>monospace<\/family>/).empty? then
        monospace << [priority, pkg.name]
      end
    end
    if sans.empty? && serif.empty? && monospace.empty? then
      other << pkg.name
    end
  end
  pkg2lang[pkg.name].each do |l|
    lang2pkg[l] ||= {}
    lang2pkg[l][:sans] ||= []
    lang2pkg[l][:serif] ||= []
    lang2pkg[l][:monospace] ||= []
    lang2pkg[l][:other] ||= []
    STDERR.printf("  Aliases[%s]: ", l)
    unless sans.empty?
      lang2pkg[l][:sans].push(*sans)
      STDERR.printf("[sans] ")
    end
    unless serif.empty?
      lang2pkg[l][:serif].push(*serif)
      STDERR.printf("[serif] ")
    end
    unless monospace.empty?
      lang2pkg[l][:monospace].push(*monospace)
      STDERR.printf("[monospace] ")
    end
    unless other.empty?
      lang2pkg[l][:other].push(*other)
      STDERR.printf("[other] ")
    end
    STDERR.printf("\n")
  end
end
STDERR.printf("sorting out against the priority...\n")
lang2pkg.each do |l,v|
  v.each do |k, vv|
    if k == :other then
      lang2pkg[l][k] = vv.sort
    elsif vv.length > 0 then
      lang2pkg[l][k] = vv.sort{|x,y| x[0] <=> y[0]}.map{|x| x[1]}
    end
  end
end

print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
print "<head><title>fonts list</title><style type=\"text/css\">\n"
print "table {\n"
print "  border-collapse: collapse;\n"
print "}\n"
print "table, th, td {\n"
print "  border: 1px solid black;\n"
print "}"
print "</style></head>\n"
print "<body><table><thead><tr><th>language</th><th>sans</th><th>serif</th><th>monospace</th><th>other</th></tr></thead>\n"
print "<tbody>"
lang2pkg.keys.sort.each do |l|
  print "<tr>"
  printf("<td>%s</td>", l)
  printf("<td>%s</td>", lang2pkg[l][:sans].join(', '))
  printf("<td>%s</td>", lang2pkg[l][:serif].join(', '))
  printf("<td>%s</td>", lang2pkg[l][:monospace].join(', '))
  printf("<td>%s</td>", lang2pkg[l][:other].join(', '))
  print "</tr>\n"
end
print "</tbody></table>\n"
